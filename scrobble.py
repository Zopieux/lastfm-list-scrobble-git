#!/usr/bin/env python3

import argparse
import pylast
import stagger
import time

from stagger.id3 import UFID
from stagger.frames import Frame
from pprint import pprint

LIMIT_BATCH_TO = 30  # lastfm seems to be doing shit with large batches (50)
MAX_RETRIES = 6

API_KEY = '3b5b29fbd3777e3041b8d77e1087149e'
API_SECRET = '0a5770add9f0baf63a2caf19d37b7fde'
TIMESTAMP_PAST_RANGE = (60., 3600.)  # one minute, one hour

TRACK_ATTR_MAP = (
    ('album', 'album'),
    ('album_artist', 'album-artist'),
    ('track_number', 'track'),
    ('mbid', UFID),
)


def p(*args):
    """Display function."""
    print(*args)


def p_sep():
    p("-" * 80)


def retry_wrapper(f, *args, **kwargs):
    delay = 2
    i = 0
    while i < MAX_RETRIES:
        try:
            f(*args, **kwargs)
            return
        except pylast.WSError as err:
            i += 1
            p("  Error while doing an action.")
            p("    %s" % err)
            p("  Retrying in %s seconds..." % delay)
            time.sleep(delay)
            delay *= 2
    else:
        p("Too much tries. Skipping.")


def scrobble(lastfm, opts):
    """Does the scrobbling."""
    length = len(opts.files)

    p("Scrobbling %d files." % length)
    p()

    tracks = []
    scrobbles = []

    # too much songs to fit in TIMESTAMP_PAST(max) ?
    if length * TIMESTAMP_PAST_RANGE[0] > TIMESTAMP_PAST_RANGE[1]:
        step = TIMESTAMP_PAST_RANGE[1] / length
        timestamp = time.time() - TIMESTAMP_PAST_RANGE[1]
    else:
        step = TIMESTAMP_PAST_RANGE[0]
        timestamp = time.time() - step * length

    for i, fname in enumerate(opts.files):
        p_sep()
        p("File %s" % fname)
        try:
            tag = stagger.read_tag(fname)
        except Exception as err:
            p("  Error: %s" % err)
            continue

        track = pylast.Track(tag.artist, tag.title, lastfm)
        p("  %s" % track)

        if opts.noop:
            continue

        tracks.append(track)

        attrs = {
            'artist': tag.artist,
            'title': tag.title,
            'timestamp': int(timestamp + step * i),
        }
        for key, attr in TRACK_ATTR_MAP:
            try:
                if isinstance(attr, str):
                    attrs[key] = getattr(tag, attr)
                else:
                    attrs[key] = tag[attr][0].data.decode('ascii', 'ignore')
            except (IndexError, KeyError, AttributeError) as e:
                pass

        scrobbles.append(attrs)

    if opts.scrobble:
        p()
        p_sep()
        while True:
            subset = scrobbles[ind * LIMIT_BATCH_TO:(ind + 1) * LIMIT_BATCH_TO]
            if subset == []:
                break

            p("Scrobbling batch %d..." % (ind + 1))

            retry_wrapper(lastfm.scrobble_many, subset)

    if opts.add:
        p()
        p_sep()
        p("Now adding files.")
        user = lastfm.get_authenticated_user()
        library = user.get_library()
        for track in tracks:
            p("  %s" % track)
            params = self._get_params()
            params["track"] = track.get_title()
            print(params)
            self._request("library.addTrack", False, params)

            retry_wrapper(library.add_track, track)

    if opts.love:
        p()
        p_sep()
        p("Now loving files.")
        for track in tracks:
            p("  %s" % track)
            retry_wrapper(track.love)


def main():
    parser = argparse.ArgumentParser(
        description="Scrobbles a list of files to Last.fm.")
    parser.add_argument('files', nargs='+',
        help="Files to be scrobbled.")
    parser.add_argument('-s', '--scrobble', action='store_true',
        help="Scrobble songs.")
    parser.add_argument('-l', '--love', action='store_true',
        help="Mark the scrobbled songs as `loved`.")
    # parser.add_argument('-s', '--suggest', action='store_true',
    #     help="Display ID3 correction suggestions, if any.")
    parser.add_argument('-a', '--add', action='store_true',
        help="Add tracks to collection.")
    parser.add_argument('-n', '--noop', action='store_true',
        help="Don't really do any action, just simulate.")

    # FIXME: the shitty argparse lib doesn't allow nested groups
    # Make a patch, then fix this shit, please.
    arg_group_auth = parser.add_mutually_exclusive_group(required=True)
    # Key...
    arg_group_auth.add_argument('-k', '--session-key', dest='session_key',
        help="Use this session key. If not session key is given, the program "\
        "will first authenticate.")
    # ... or user/password pair
    arg_group_auth.add_argument('-u', '--user', dest='auth_user',
        help="Login/password authentification.")
    # We *have* to put password under parser because no damn nested
    # groups. Fuck you, argparse.
    parser.add_argument('-p', '--password', dest='auth_pswd')

    opts = parser.parse_args()
    # FIXME: will become useless when argparse is fixed
    if opts.auth_user and not opts.auth_pswd:
        parser.error("You must provide a password along with the user.")

    client = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET,
        session_key=opts.session_key,
        username=opts.auth_user, password_hash=pylast.md5(opts.auth_pswd))

    scrobble(client, opts)

if __name__ == '__main__':
    main()
